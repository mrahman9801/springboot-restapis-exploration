package com.muhammad.springboot.projects.restfulwebservices.hello;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

// REST API
@RestController
public class HelloController {
	
	private MessageSource messageSource;
	
	public HelloController(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	// Paths
	@GetMapping(path="/hello")
	public String Hello() {
		return "Hello";
	}
	
	@GetMapping(path="/hello-bean")
	public HelloBean helloBean() {
		// Autoconfigures beans into JSON format
		return new HelloBean("Sup");
	}
	
	@GetMapping(path="/hello-bean/{name}")
	public HelloBean helloPathVariable(@PathVariable String name) {
		// Autoconfigures beans into JSON format
		return new HelloBean(String.format("Sup, %s", name));
	}
	
	// Checks Accept-Language Field in headers with a default message.
	// Need to specify messages in src/main/resources.
	@GetMapping(path="/hello-internationalized")
	public String HelloIntertionalized() {
		Locale locale = LocaleContextHolder.getLocale();
		return messageSource.getMessage("good.morning.message", null, "Default Message", locale);
	}
	
}
