package com.muhammad.springboot.projects.restfulwebservices.user;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.muhammad.springboot.projects.restfulwebservices.jpa.PostRepository;
import com.muhammad.springboot.projects.restfulwebservices.jpa.UserRepository;

import jakarta.validation.Valid;

@RestController
public class UserJpaResource {

	private UserRepository repository;

	private PostRepository postRepository;

	public UserJpaResource(UserRepository repository, PostRepository postRepository) {
		this.repository = repository;
		this.postRepository = postRepository;
	}

	@GetMapping("/jpa/users")
	public List<User> getAllUsers() {
//		return service.findAll();
		return repository.findAll();
	}
	
//	@GetMapping("/jpa/users")
//	public MappingJacksonValue getAllUsers() {
////		return service.findAll();
//		List<User> usersList = repository.findAll();
//		
//		MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(usersList);
	// hide id field
//		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("user_name", "birthDate");
//		FilterProvider filters = new SimpleFilterProvider().addFilter("UserFilter", filter);
//		mappingJacksonValue.setFilters(filters);
//		
//		return mappingJacksonValue;
//	}

	// HATEOS Hypdermedia as the Engine of Application State
	// Consumers can see data and how they can perform extra actions
	// HAL (JSON Hypertext Application Language) simple, consistent format
	// Need starter dependency
	@GetMapping("/jpa/users/{id}") // Give them a specific user and link to all users
	public EntityModel<User> getUserById(@PathVariable int id) {
		Optional<User> user = repository.findById(id);

		if (user.isEmpty()) {
			throw new UserNotFoundException("User with id:" + id + " does not exist.");
		}

		EntityModel<User> entityModel = EntityModel.of(user.get());

		WebMvcLinkBuilder link = linkTo(methodOn(this.getClass()).getAllUsers());
		entityModel.add(link.withRel("all-users"));

		return entityModel;
	}

	@PostMapping("/jpa/users")
	public ResponseEntity<User> createUser(@Valid @RequestBody User user) {
//		User savedUser = service.addUser(user);
		User savedUser = repository.save(user);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedUser.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@DeleteMapping("/jpa/users/{id}")
	public void deleteUser(@PathVariable int id) {
//		service.deleteUserById(id);
		repository.deleteById(id);
	}

	@GetMapping("/jpa/users/{id}/posts")
	public List<Post> getPostsOfUser(@PathVariable int id) {
		Optional<User> user = repository.findById(id);

		if (user.isEmpty()) {
			throw new UserNotFoundException("User with id:" + id + " does not exist.");
		}
		return user.get().getPosts();
	}

	@PostMapping("/jpa/users/{id}/posts")
	public ResponseEntity<Post> createPostForUser(@PathVariable int id, @Valid @RequestBody Post post) {
		Optional<User> user = repository.findById(id);

		if (user.isEmpty()) {
			throw new UserNotFoundException("User with id:" + id + " does not exist.");
		}

		post.setUser(user.get());

		Post savedPost = postRepository.save(post);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedPost.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@GetMapping("/jpa/users/{user_id}/posts/{id}") // Give them a specific user and link to all users
	public EntityModel<Post> getPostById(@PathVariable int user_id, @PathVariable int id) {

		Optional<User> user = repository.findById(user_id);
		if (user.isEmpty()) {
			throw new UserNotFoundException("User with id:" + id + " does not exist.");
		}

		Optional<Post> post = postRepository.findById(id);
		if (post.isEmpty()) {
			throw new PostNotFoundException("Post with id:" + id + " does not exist.");
		}

		EntityModel<Post> entityModel = EntityModel.of(post.get());

		WebMvcLinkBuilder link = linkTo(methodOn(this.getClass()).getPostsOfUser(user_id));
		entityModel.add(link.withRel("all-posts-of-user"));

		return entityModel;
	}
}
