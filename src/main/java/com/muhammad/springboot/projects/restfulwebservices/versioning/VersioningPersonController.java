package com.muhammad.springboot.projects.restfulwebservices.versioning;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersioningPersonController {
	
	// Twitter does this: URI versioning
	
	@GetMapping("/v1/person")
	public PersonV1 getFirstVersionOfPerson() {
		return new PersonV1("Charles Adams");
	}
	
	@GetMapping("/v2/person")
	public PersonV2 getSecondVersionOfPerson() {
		return new PersonV2(new Name("Charles", "Anderson"));
	}
	
	// Amazon: Request Parameters versioning
	
	@GetMapping(path="/person", params="version=1")
	public PersonV1 getFirstVersionOfPersonRequestParameter() {
		return new PersonV1("Charles Adams");
	}
	
	@GetMapping(path="/person", params="version=2")
	public PersonV2 getSecondVersionOfPersonRequestParameter() {
		return new PersonV2(new Name("Charles", "Anderson"));
	}
	
	// Microsoft: (Custom) Headers versioning:
	
	@GetMapping(path="/person/header", headers="X-API-VERSION=1")
	public PersonV1 getFirstVersionOfPersonHeaders() {
		return new PersonV1("Charles Adams");
	}
	
	@GetMapping(path="/person/header", headers="X-API-VERSION=2")
	public PersonV2 getSecondVersionOfPersonHeaders() {
		return new PersonV2(new Name("Charles", "Anderson"));
	}
	
	// Github: Media type versioning / content negotiation / accept header
	
	@GetMapping(path="/person/accept", produces="application/vnd.company.app-v1+json")
	public PersonV1 getFirstVersionOfAcceptHeaders() {
		return new PersonV1("Charles Adams");
	}
	
	@GetMapping(path="/person/accept", produces="application/vnd.company.app-v2+json")
	public PersonV2 getSecondVersionOfAcceptHeaders() {
		return new PersonV2(new Name("Charles", "Anderson"));
	}
	
	// Tradeoffs for all. URI pollution with first two. HTTP headers were
	// not meant to do versioning like last two do.
	// First two can easily do on browser, but last two need manipulation of headers.
	// API documentation easier with URLs based versioning.
	// Be consistent with your enterprise!!!!!!!!!!!!!!!
	
}
