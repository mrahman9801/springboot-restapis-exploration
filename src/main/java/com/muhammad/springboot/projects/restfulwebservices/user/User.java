package com.muhammad.springboot.projects.restfulwebservices.user;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Size;

@Entity(name = "user_details") // we change the table name because user is a keyword in h2
//@JsonFilter("UserFilter")
public class User {

	@Id
	@GeneratedValue
	private Integer Id;

	@Size(min = 2, message = "Name needs atleast 2 characters.")
	@JsonProperty("user_name") // Changes fieldname in JSON response: name-->user_name
	private String name; // in a post request you would also have to send user_name not name as a field

	@Past(message = "Birth date needs to be in the past.")
	private LocalDate birthDate; // this column will be made as birth_date (blame database maker lol)

	// list of posts belonging to user
	@OneToMany(mappedBy = "user") // by what property on posts? the user property
	@JsonIgnore
	private List<Post> posts;

	// need a default contructor fallback
	protected User() {

	}

	public User(Integer id, String name, LocalDate birthDate) {
		super();
		Id = id;
		this.name = name;
		this.birthDate = birthDate;
	}

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	@Override
	public String toString() {
		return "User [Id=" + Id + ", name=" + name + ", birthDate=" + birthDate + "]";
	}

}
