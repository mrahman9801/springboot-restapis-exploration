### Quickstart examples for building REST APIs with Spring Boot  

Use this as quick reference for building.  

#### Coverage:

1. Versioning methodologies  
2. Content Negotiation  
3. Custom exceptions by overriding default exceptions  
4. JSON filtering  
5. Mapping entity relationships  
6. Configuring security  
7. Integrating MySQL Docker container  

